#!/bin/bash

# Prompt the user to enter the directory path
echo "Enter the directory path:"
read directory_path

# Check if the entered directory path exists
if [ -d "$directory_path" ]; then
    # List files in the directory
    echo "Listing files in $directory_path:"
    ls -l "$directory_path"
else
    echo "Directory does not exist."
fi
