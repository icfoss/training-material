import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go

# Function to create bar chart
def create_bar_chart(df):
    # Set the 'District' column as the index
    df.set_index('District', inplace=True)

    # Create a bar plot
    ax = df.plot(kind='bar', figsize=(10, 6))

    # Adding title and labels to the plot
    plt.title('Birth Rate, Death Rate, and Infant Death Rate by District')
    plt.xlabel('District')
    plt.ylabel('Rate')

    # Rotate x-axis labels by 45 degrees
    plt.xticks(rotation=45, ha='right')

    # Adding legend to the plot with a title and specifying the location
    plt.legend(title='Rates', loc='upper right')

    # Display the plot using st.pyplot() with the current Matplotlib figure
    st.pyplot(plt.gcf())



# Function to create funnel chart
def create_funnel_chart(df):
    # Sort the DataFrame by Infant Death Rate in descending order
    df = df.sort_values(by='Infant Death Rate', ascending=False)

    # Create a funnel chart
    fig = go.Figure()

    fig.add_trace(go.Funnel(
        name='Birth Rate',
        y=df['District'],
        x=df['Birth Rate'],
        hoverinfo='none'
    ))

    fig.add_trace(go.Funnel(
        name='Death Rate',
        orientation='h',
        y=df['District'],
        x=df['Death Rate'],
        hoverinfo='none'
    ))

    fig.add_trace(go.Funnel(
        name='Infant Death Rate',
        orientation='h',
        y=df['District'],
        x=df['Infant Death Rate'],
        hoverinfo='none'
    ))

    fig.update_layout(margin=dict(l=0, r=0, b=0, t=40))

    # Display the plot using st.plotly_chart()
    st.plotly_chart(fig)

# Function to create sunburst chart
def create_sunburst_chart(df):
    # Melt the dataframe for easy plotting
    df_melted = pd.melt(df, id_vars=['District'], var_name='Variable', value_name='Value')

    # Create sunburst chart
    fig = px.sunburst(
        df_melted,
        path=['District', 'Variable'],
        values='Value',
    )

    # Display the plot using st.plotly_chart()
    st.plotly_chart(fig)

# Main Streamlit app
def main():
    st.title("Data Visualisation")

    # Use st.sidebar for file uploading
    uploaded_file = st.sidebar.file_uploader("Choose a CSV file", type="csv")

    if uploaded_file is not None:
        # Load CSV data
        df = pd.read_csv(uploaded_file)

        # Display buttons for each chart
        if st.sidebar.button("1. Show Bar Chart"):
            st.header("Bar Chart")
            create_bar_chart(df)

        if st.sidebar.button("2. Show Funnel Chart"):
            st.header("Funnel Chart")
            create_funnel_chart(df)

        if st.sidebar.button("3. Show Sunburst Chart"):
            st.header("Sunburst Chart")
            create_sunburst_chart(df)

if __name__ == "__main__":
    main()
