import json
with open('student_record.json', 'r') as file:
    data = json.load(file)
#print(data)
student_names = [student["name"] for student in data["students"]]
print("Student Names:", student_names)
student_names.append("")
print("After append:",student_names)
student_names.insert(2,"")
print("After Inserting at 2",student_names)
student_names.remove("")
print("After deletion",student_names)
removed_student = student_names.pop(2)
print("Removed student",removed_student)
temp_names=student_names.copy()
temp_names.clear()
print("After Clear:", temp_names)
index_of_student = student_names.index("Jane Smith")
print("Index of 'Jane Smith':", index_of_student)

# 8. count() - Return the number of times x appears in the list
count_of_student = student_names.count("Alice Johnson")
print("Count of 'Alice Johnson':", count_of_student)

# 9. sort() - Sort the items of the list in place
student_names.sort()
print("After sort:", student_names)

# 10. reverse() - Reverse the elements of the list in place
student_names.reverse()
print("After reverse:", student_names)

# 11. copy() - Return a shallow copy of the list
copied_list = student_names.copy()
print("Copied List:", copied_list)




student_tuples = [(student["student_id"],student["name"],student["age"],student["major"],tuple(student["courses"]),
tuple(student["grades"].items()))  for student in data["students"]]
print("Student Tuples:", student_tuples)

# Demonstrating tuple use cases

# 1. Accessing elements
first_student = student_tuples[0]
print("First Student:", first_student)
print("First Student Name:", first_student[1])

# 2. Tuple unpacking
student_id, name, age, major, courses, grades = first_student
print("Unpacked Student:")
print("ID:", student_id)
print("Name:", name)
print("Age:", age)
print("Major:", major)
print("Courses:", courses)
print("Grades:", grades)

# 3. Using tuples as dictionary keys
student_dict = { (student[0], student[1]): student for student in student_tuples }
print("Student Dictionary Keys:", student_dict.keys())
print("Student Data for Key (1, 'John Doe'):", student_dict[(1, 'John Doe')])

# 4. Nested tuples
nested_tuple = (student_id, (name, age), (major, courses, grades))
print("Nested Tuple:", nested_tuple)

# 5. Immutable nature
# Trying to modify the tuple will raise an error
try:
    first_student[1] = "New Name"
except TypeError as e:
    print("Error: Tuples are immutable -", e)
    
    
# Creating a dictionary for each student with student_id as the key
students_dict = {student["student_id"]: student for student in data["students"]}
print("Student Dictionary:", students_dict)

# Demonstrating dictionary methods

# 1. get() - Return the value for the specified key if the key is in the dictionary
student_1 = students_dict.get(1)
print("Get Student with ID 1:", student_1)

# 2. keys() - Return a new view of the dictionary's keys
keys = students_dict.keys()
print("Keys:", keys)

# 3. values() - Return a new view of the dictionary's values
values = students_dict.values()
print("Values:", values)

# 4. items() - Return a new view of the dictionary's items (key, value pairs)
items = students_dict.items()
print("Items:", items)

# 5. update() - Update the dictionary with elements from another dictionary object or from an iterable of key/value pairs
students_dict.update({3: {"student_id": 3, "name": "Alice Johnson", "age": 21, "major": "Physics"}})
print("After Update:", students_dict)

# 6. pop() - Remove the specified key and return the corresponding value
popped_student = students_dict.pop(3)
print("Popped Student:", popped_student)
print("After Pop:", students_dict)

# 7. popitem() - Remove and return a (key, value) pair from the dictionary
popped_item = students_dict.popitem()
print("Popped Item:", popped_item)
print("After Popitem:", students_dict)

# 8. setdefault() - Return the value of the specified key. If the key does not exist, insert the key with the specified value
student_2 = students_dict.setdefault(2, {"student_id": 2, "name": "New Student"})
print("Set Default for ID 2:", student_2)
print("After SetDefault:", students_dict)

# 9. copy() - Return a shallow copy of the dictionary
copied_dict = students_dict.copy()
print("Copied Dictionary:", copied_dict)

# 10. clear() - Remove all items from the dictionary
cleared_dict = students_dict.copy()
cleared_dict.clear()
print("After Clear:", cleared_dict)
